#!/usr/bin/env nextflow

process mudskipper {
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(run) - Run Name
//     val(dataset) - Dataset
//     path(bam) - Alignment
//   path(gtf) - GTF File
//   val parstr - Additoinal Parameters
//
// output:
//   tuple => emit: quants
//       val(pat_name) - Patient Name
//       val(run) - Run Name
//       val(dataset) - Dataset
//       path('*txome.bam') - Transcriptome BAM

// require:
//   FQS
//   IDX_FILES
//   params.salmon$salmon_map_quant_parameters

  tag "${dataset}/${pat_name}/${run}"
  label 'mudskipper_container'
  label 'mudskipper'
//  publishDir "${params.samps_out_dir}/${dataset}/${pat_name}/${run}/mudskipper"
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(bam)
  path gtf
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path("*txome.bam"), emit: txome_bams

  script:
  """
  mudskipper bulk \
  -a ${bam} \
  -g ${gtf} \
  -o ${dataset}-${pat_name}-${run_name}.txome.bam \
  -t ${task.cpus} \
  -l
  """
}
